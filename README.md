# Overview
This project is a collection of experimental scripts for jetson + MIPI camera
Previously:
This project is the implementation of anonymization and yolov4 on Jetson.

The main code is in vision.py.

# Setup
- install OpenCV for python. Follow nVidia instructions.

# How to run
Running with default settings
```
$ python vision.py
```

Run with custom settings
```
$ python vision.py --delay 1 --save 1 --camera_id 1
```
- 1 second delay between each image
- save data on server
- using camera 1

Get help
```
$ python vision.py --help
```
# Camera configuration
(fokallængde i mm) * (billedbredde i pixels 1280)  / (sensorbredde i mm)

Information for finding correct sensor size for arducam cameras.
https://www.arducam.com/focal-length-calculator

3.15 * 1280 / 3.6
= 1120

# Robot controls
motion_control.py; instantiate the class and use Move.to() to drive the robot around.