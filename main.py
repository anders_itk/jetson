import json
import os
import requests
import urllib.parse
from custom_logger import logger
import paho.mqtt.client as mqtt
from config import Development as config
from time import sleep

def usb_capture_device():
    """
    Get capture device
    :return: cv2.VideoCapture
    """
    # set required ENV var, in some cases...
    os.putenv("OPENBLAS_CORETYPE","ARMV8")
    import cv2
    print(cv2.__file__) # ensure correct import.
    # at time of writing it prints '/home/itk/.virtualenvs/anonymizer/lib/python3.6/site-packages/cv2/cv2.cpython-36m-aarch64-linux-gnu.so'
    cam = cv2.VideoCapture(0)
    return cam

class MQTT_client():
    """
    Subscribes to GNSS coordinates from MQTT server.
    Notice: We don't know if the connection to broker/producer is lost. No way to know if connection is lost without sending a message to broker.

    :param ignore_staet: Boolean, if set to false, then the object will react if it cannot connect to broker.

    Usage:
    client = MQTT_client()
    # loop is startet automatically

    # get message from broker
    print(client.latest_message)

    client.loop_stop()
    """
    def __init__(self, ignore_state: bool = True) -> None:
        self.client = mqtt.Client(config.MQTT_CLIENT_NAME)
        self.latest_message = None
        for _ in range(3):
            try:
                self.client.connect(config.BROKER_IP)
            except:
                print("Unable to connect to MQTT broker. Retrying...")
                sleep(2)
            else:
                self.client.subscribe("fix")
                self.client.on_message = self.on_message
                break
        logger.info("Starting MQTT loop")
        self.client.loop_start()
        self.client_connected(ignore_state)



    def stop(self):
        logger.info("Stopping MQTT loop.")
        self.client.loop_stop()

    def on_message(self, client, userdata, message):
        # TODO consider thread locks on self.message. But the operation should be atomic so yolo.
        self.latest_message = message
        #print("message received ", str(message.payload.decode("utf-8")))
        #print("message topic=", message.topic)
        #print("message qos=", message.qos)
        #print("message retain flag=", message.retain)

    def client_connected(self, ignore_state: bool):
        """
        Decide how to act if the client cannot connect.
        """
        sleep(1)
        if self.client._state == 0:
            for _ in range(3): print("\n")
            sleep(0.1)
            print("Could not connect to MQTT broker.")
            sleep(0.1)

            if ignore_state:
                self.latest_message = mqtt.MQTTMessage()
                self.latest_message.payload = b"N/A"

            for _ in range(3): print("\n")
            sleep(3)


class Inference():
    def __init__(self, cam_data):
        """
        Usage example:

        inf = Inference(config.PRIMARY_CAM)
        result = inf.remote_prediction(buffer=buffer)

        :param cam_data: string formatted json with camera configuration
        """
        json.loads(cam_data) # validate string formatting.
        logger.info(f"Camera config: {cam_data}")
        self.url_cam_data = urllib.parse.quote_plus(cam_data)

    def remote_prediction(self, img, gnss: str = "", label: str = "", persist: bool = False) -> str:
        """
        POST request to Paperspace. The request contains a .jpg and camera data. Gets tracklet coordinates in camera space.
        :param img: image as binary/file-like object
        :param gnss: string with location information
        :param label: string for naming files on server. Should be file-system friendly. Only used withen persist = True.
        :param persist: boolean, if true then image, gnss, and buffer image is stored on server file system.
        :return: string formatted json with predictions. E.g. '{"data":{"bounding-boxes":[]},"error":"","success":true}'
        """
        url = "http://74.82.29.29:57744/models/cig/predict_plane?camera_data=" + self.url_cam_data

        payload = {'gnss': gnss,
                   'label': label,
                   'persist': persist}
        files = [
            ('input_data', ('',img,'image/jpeg')) # filename, data buffer, MIME-type?
        ]
        headers = {}

        response = requests.request("POST", url, headers=headers, data=payload, files=files)
        return response.text

    def remote_prediction_flat(self, img, gnss: str = "", label: str = "", persist: bool = False) -> str:
        """
        POST request to Paperspace. The request contains a .jpg and camera data. Gets tracklet 2D coordinates in image.
        :param img: image as binary/file-like object
        :param label: string for naming files on server. Should be file-system friendly. Only used withen persist = True.
        :param persist: boolean, if true then image, gnss, and buffer image is stored on server file system.
        :return: string formatted json with predictions. E.g. '{"data":{"bounding-boxes":[]},"error":"","success":true}'
        """
        url = "http://74.82.29.29:57744/models/cig/predict"

        payload = {'gnss': gnss,
                   'label': label,
                   'persist': persist}
        files = [
            ('input_data', ('',img,'image/jpeg')) # filename, data buffer, MIME-type?
        ]
        headers = {}

        response = requests.request("POST", url, headers=headers, data=payload, files=files)
        return response.text

    def live_demo(self, img):
        """
        Gets image with markings from remote server.
        Start `$ eog result.jpg` to see results
        :param img: .jpg image of cigarette butts
        :return: .PNG with drawn bounding boxes
        """
        url = "http://74.82.29.29:57744/models/cig/predict_image"

        payload = {}
        files = [
            ('input_data', ('', img, 'image/jpeg'))  # filename, data buffer, MIME-type?
        ]
        headers = {}

        response = requests.request("POST", url, headers=headers, data=payload, files=files)
        return response



if __name__ == '__main__':
    """
    Anders Krogsager
    andkr@aarhus.dk
    30/08/2021
    
    version 0.1 requires python 3.6
    
    inspiration:
    https://www.arducam.com/docs/arducam-obisp-mipi-camera-module/4-use-on-jetson-nano/4-4-live-view-the-video-using-opencv-with-python/
    
    """
    cam = usb_capture_device()
    _, frame = cam.read()
    print("cam read "+str(_))
    #cv2.imshow('image', frame) # show image in gui
    filename = os.getcwd()+os.sep+"cap/0.jpg" # REMEMBER TO MANUALLY CREATE DIR


    _, buffer = cv2.imencode('.jpg', frame)

    # write buffer to file
    file=open('/home/itk/jetson_skodrobot/cap/my_cool_buffer.jpg', 'wb')
    file.write(buffer)
    file.close()

    # write image (npArray) to file
    #cv2.imwrite(filename=filename,
    #            img=frame)
    cam.release() #clean up

    #remote_prediction(buffer)

    exit(0)