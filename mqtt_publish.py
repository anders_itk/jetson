import paho.mqtt.publish as publish
from config import Development as config

payload = """
{

"timestamp":"2021-09-23 14:0:51",

"path_uuid":"Path0",

"map_uuid":0,

"position_encoding":0,

"nodes":[

{"uuid":"node_0","sequence_number":0,"position":{"x":56.153115,"y":10.215197,"z":117.01000122074038},"actions":[]},

{"uuid":"node_1","sequence_number":1,"position":{"x":56.153069,"y":10.215233,"z":117.01000804826617},"actions":[]},

],

"edges":[

{"uuid":"edge_0","start_node_uuid":"node_0","end_node_uuid":"node_1","actions":[{"uuid”: "drive", "parameters”: [{"key”: "speed", "value_float32”: 0.1}]}]}
]

}
"""

no_speed_payload = """
{

"timestamp":"2021-09-23 14:0:51",

"path_uuid":"Path0",

"map_uuid":0,

"position_encoding":0,

"nodes":[

{"uuid":"node_0","sequence_number":0,"position":{"x":56.153115,"y":10.215197,"z":117.01000122074038},"actions":[]},

{"uuid":"node_1","sequence_number":1,"position":{"x":56.153069,"y":10.215233,"z":117.01000804826617},"actions":[]},

],

"edges":[

{"uuid":"edge_0","start_node_uuid":"node_0","end_node_uuid":"node_1","actions":[]}

}
"""


payload = payload.replace('\n','')
print(payload)
publish.single("capra/navigation/send_path", "payload", hostname=config.BROKER_IP)
