import Jetson.GPIO as GPIO
from time import sleep
import pdb

# setup
wheel_up_down = 16
hoover = 18
wheel_down_up = 22
GPIO.setmode(GPIO.BOARD)
GPIO.setup(16,GPIO.OUT)
GPIO.setup(18,GPIO.OUT)
GPIO.setup(22,GPIO.OUT)

print("ready to hoover for 3 seconds")
pdb.set_trace()

# activate equipment.
GPIO.output(hoover, GPIO.HIGH)
sleep(3)
GPIO.output(hoover, GPIO.LOW)

pdb.set_trace()