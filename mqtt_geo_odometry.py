from main import MQTT_client
from time import sleep
import pdb

if __name__ == "__main__":
    """
    This script is useful for testing the MQTT producer.
    """
    mc = MQTT_client(source='capra/robot/geo_odometry')
    while(True):
        sleep(1)
        print(mc.latest_message.payload.decode())
        print(mc.latest_message.timestamp)