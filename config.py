class Development():

    # Camera setup
    PRIMARY_CAM = '{"angle": 43, "image_size": [1280, 720], "focal_length_px": 1120, "height": 0.475}' # ARDUCAM IMX 219 UC-609, Rev A.
    CAM_ROTATION = 0

    # MQTT Client
    MQTT_CLIENT_NAME = "vision_system"
    BROKER_IP = "10.46.28.1"
