import json
import signal
import argparse

import pdb
import math
from custom_logger import logger
import time
from mipi_cap import get_mipi_capture_device
from config import Development as config
import cv2
from main import Inference, MQTT_client
from io import BytesIO
from motion_control import Move

parser = argparse.ArgumentParser(description='Start camera loop. Stop with Ctrl+c.',formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('--camera_id', type=int, default=0, choices=[0,1],
                    help='Select the camera by id.')
parser.add_argument('--save', type=int, default=0, choices=[0,1],
                    help='If set to 1, then data is saved on remote server.')
parser.add_argument('--delay', type=int, default=5,
                    help='Seconds delay between each picture is taken.')
parser.add_argument('--ignore_mqtt', type=int, default=0, choices=[0,1],
                    help='Set to 1 to ignore if MQTT client cannot connect.')

args = parser.parse_args()

class Main():
    """
    Stop process with Ctrl+C.
    """

    def __init__(self):
        self.run = True
        signal.signal(signal.SIGINT, self.handler)

        # start mqtt client
        self.mc = MQTT_client(ignore_state=bool(args.ignore_mqtt))
        if self.mc.client._state == 0:
            if bool(args.ignore_mqtt):
                logger.warning("Not listening to MQTT.")
            else:
                exit(0)
        # get camera
        self.cam = get_mipi_capture_device(cam_rotation=config.CAM_ROTATION, sensor_id=args.camera_id)

        # prepare for inference
        self.inference = Inference(config.PRIMARY_CAM)

        # prepare for movement
        self.move = Move()


    def handler(self,signum, frame):
        self.run=False

    def start(self):
        logger.info("Starting main loop")
        image_width_px = 1280  # get from camera
        half_width = round(image_width_px / 2)

        while self.run:
            timer = time.time()
            read_ok, frame = self.cam.read()
            if read_ok:
                _, buffer = cv2.imencode('.jpg', frame)
                gnss = self.format_mqtt_location(self.mc.latest_message)
                # TODO change back to remote_rediction()
                resp = self.inference.remote_prediction_flat(img=BytesIO(buffer),
                                                        persist=bool(args.save),
                                                        gnss=gnss,
                                                        label=time.strftime('%Y-%m-%d_%H.%M.%S', time.localtime(timer))
                                                        )

                resp = json.loads(resp)
                # logger.debug(self.format_resp(resp=resp)) # TODO enable "ingen cigaretter fundet"

                try:
                    #get the object closest to the buttom of the images
                    left, right = get_closet_object(resp)
                    print(left, right)
                    # calculate radian from object position & image width
                    # utter right/left gives +-90 degrees in rad.
                    tracklet_coordinate_horisontal_px = round((right + left) / 2)
                    radian = ((half_width - tracklet_coordinate_horisontal_px) / half_width) * 1.57
                    radian = round(radian, 3)
                    print(radian)
                    self.move.to(duration=0.8, velocity=0.08, angle=radian)
                except Exception as e:
                    print("got nothing", resp['data']['bounding-boxes'])
                    print(e)

            else:
                logger.critical("Failed to read image from camera.")

            # loiter until delay time is reached
            while (time.time() < timer+float(args.delay)) and self.run:
                self.cam.grab() # waste buffer frames
                """
                it’s always the same solution            
                spawn a thread
                thread reads frames
                thread keeps latest frame around
                other code uses latest frame (or waits for an update)


                or check
                https://stackoverflow.com/questions/43665208/how-to-get-the-latest-frame-from-capture-device-camera-in-opencv
                """

        # done. Remember to clean up.
        logger.info("Stopping main loop.")

    def clean_up(self):
        logger.info("Clean up.")
        self.cam.release()
        self.mc.stop()

    def format_resp(self, resp: dict) -> str:
        """
        The inference server results is a complex json file with coordinates of results.
        Format the json resonse from inference server, and return a human readable result.
        :param resp: json
        :return: human readable string
        """
        if (len(resp['data']['bounding-boxes']) == 0):
            return "Ingen cigaretter fundet"
        else:
            return resp['data']['bounding-boxes'][0]['point_in_plane']

    def format_mqtt_location(self, message) -> str:
        """
        Format the raw message from MQTT to something useful for later analysis.
        :param message: import paho.mqtt.client.MQTTMessage()
        :return: string

        Example message:
        {'version': 1, 'refStationId': 0, 'iTow': 15011000, 'relPosN': 0, 'relPosE': 0, 'relPosD': 0, 'relPosLength': 0, 'relPosHeading': 0, 'relPosHPN': 0, 'relPosHPE': 0, 'relPosHPD': 0, 'relPosHPLength': 0, 'accN': 0, 'accE': 0, 'accD': 0, 'accLength': 0, 'accHeading': 0, 'flags': 0}
        """
        try:
            m = json.loads(message.payload.decode())
            # TODO m['whatever']
            return json.dumps(m) + " Message timestamp: " + str(message.timestamp)
        except:
            return "[N/A]"


def get_closet_object(response: json) -> tuple:
    """
    # poor, quick-n-dirty approximation of the object closest to the robot.
    Given a list of objects, determine which object is closest to the robot,
    and return the bounding box left and right side (in pixels).
    :param object_list:
    :return: tuple: (int, int). (left, right) Bounding box left and right pixels.
    """

    obj_centroids = []
    for obj in response['data']['bounding-boxes']:
        left = obj['coordinates']['left']
        right = obj['coordinates']['right']
        top = obj['coordinates']['top']
        bottom = obj['coordinates']['bottom']
        centroid_y = round((right + left) / 2)
        centroid_x = round((bottom + top) / 2)
        obj_centroids.append((obj, afstand(centroid_x, centroid_y)))  # TODO find manually tested files in test_visual_servoing.py

    sorted(obj_centroids, key=lambda centroid: centroid[1])
    closest_obj = obj_centroids[0][0]
    return closest_obj['coordinates']['left'], closest_obj['coordinates']['right']


def afstand(x, y):
    # euclidian distance from center bottom of image
    return math.sqrt(((x - 640) ** 2) + ((y - 720) ** 2))


if __name__ == "__main__":
    logger.info(f"Starting with args {args}")
    main = Main()
    try:
        main.start()
    finally:
        main.clean_up()

    exit(0)