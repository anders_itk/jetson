from main import MQTT_client
from time import sleep
import pdb

if __name__ == "__main__":
    """
    This script is useful for testing the MQTT producer.
    """
    mc = MQTT_client()
    sleep(0.5)
    print(mc.latest_message.payload.decode())
    print(mc.latest_message.timestamp)
    pdb.set_trace()


