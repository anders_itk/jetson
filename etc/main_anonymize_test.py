# This is a sample Python script.

# Press ⇧F10 to execute it or replace it with your code.
# Press Double ⇧ to search everywhere for classes, files, tool windows, actions, and settings.
import time
import os
import sys
sys.path.insert(0, '/home/itk/anonymizer')
import anonymizer.bin.anonymize_edit as x

def get_image_file(path: str):
    """
    One of multiple methods for retreiving an image file for processing. The tester is a .jpeg.
    Output is always the same data type which is compatible with OpenCV.
    :param path: path to image file
    :return: binary object compatible with OpenCV ????? MAT OR npArray?
    """

# Press the green button in the gutter to run the script.

def run_anon() -> str:
    images = '/home/itk/anonymizer/images/'
    images_anon = '/home/itk/anonymizer/images_anonymized/'
    weights = '/home/itk/anonymizer/weights/'

    time_start = time.time()
    x.main(input_path=images, image_output_path=images_anon, weights_path=weights, image_extensions='jpg', face_threshold=0.3, plate_threshold=0.3, obfuscation_parameters='21,2,9', write_json=True)

    runtime = (time.time()-time_start)

    return runtime

if __name__ == '__main__':
    """
    Anders Krogsager
    andkr@aarhus.dk
    30/08/2021
    
    version 0.1 requires python 3.6
    
    inspiration:
    https://www.arducam.com/docs/arducam-obisp-mipi-camera-module/4-use-on-jetson-nano/4-4-live-view-the-video-using-opencv-with-python/
    
    
    """
    # set required ENV var
    os.putenv("OPENBLAS_CORETYPE","ARMV8")
    import cv2
    print(cv2.__file__) # ensure correct import.

    # SKIP get image
    # TODO kald anonymizer
    runs = []
    runs.append(run_anon())
    runs.append(run_anon())
    runs.append(run_anon())

    print(runs)

    # bare kald terminal fra python uden at bruge import? Nej det er for langsomt.
    #
    # TODO kald yolo over REST




# See PyCharm help at https://www.jetbrains.com/help/pycharm/
