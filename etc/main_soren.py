import pdb
import time
import os
import sys
import requests
import time
import json
import cv2
import urllib.parse
import paho.mqtt.client as mqtt


def remote_prediction(buffer):
    """
    POST request to Paperspace. The request contains a .jpg and camera data.
    :return: json with predictions
    """
    url = "http://74.82.29.29:57744/models/cig/predict_plane?camera_data=%7B%22angle%22%3A%2045%2C%20%22image_size%22%3A%20%5B800%2C%20600%5D%2C%20%22focal_length_px%22%3A%201221%2C%20%22height%22%3A%2011.1%7D%20"
    
    #microsoft HD
    url = "http://74.82.29.29:57744/models/cig/predict_plane?camera_data=%7B%22angle%22%3A%2045%2C%20%22image_size%22%3A%20%5B640%2C%20480%5D%2C%20%22focal_length_px%22%3A%20310%2C%20%22height%22%3A%200.79%7D%20"

    values = urllib.parse.quote_plus('{"angle": 45, "image_size": [640, 480], "focal_length_px": 500, "height": 0.79}')
    url = "http://74.82.29.29:57744/models/cig/predict_plane?camera_data=" + values

    payload = {}
    files = [
        ('input_data', ('',buffer,'image/jpeg')) # filename, data buffer, MIME-type?
    ]
    headers = {}

    response = requests.request("POST", url, headers=headers, data=payload, files=files).json()
   
   # print(response.text)
    return response


def on_message(client, userdata, message):
    print("message received " ,str(message.payload.decode("utf-8")))
    print("message topic=",message.topic)
    print("message qos=",message.qos)
    print("message retain flag=",message.retain)


if __name__ == '__main__':


    broker_address="10.46.28.1" 
    #broker_address="iot.eclipse.org"
    print("creating new instance")
    client = mqtt.Client("P1") #create new instance
    print("connecting to broker")
    client.connect(broker_address) #connect to broker
    #print("Subscribing to topic","house/bulbs/bulb1")
    client.subscribe("navrelposned")
    client.on_message=on_message 
    client.loop_start()
    #print("Publishing message to topic","house/bulbs/bulb1")
    #client.publish("house/bulbs/bulb1","OFF")

    """
    Anders Krogsager
    andkr@aarhus.dk
    30/08/2021
    
    version 0.1 requires python 3.6
    
    inspiration:
    https://www.arducam.com/docs/arducam-obisp-mipi-camera-module/4-use-on-jetson-nano/4-4-live-view-the-video-using-opencv-with-python/
    
    """
    #print(urllib.parse.quote_plus('{"angle": 45, "image_size": [640, 480], "focal_length_px": 310, "height": 0.79}'))
    #exit(0)

    # set required ENV var
    #os.putenv("OPENBLAS_CORETYPE","ARMV8")
    #print(cv2.__file__) # ensure correct import.
    # at time of writing it prints '/home/itk/.virtualenvs/anonymizer/lib/python3.6/site-packages/cv2/cv2.cpython-36m-aarch64-linux-gnu.so'
    cam = cv2.VideoCapture(0) 

    while(True):
        #cam = cv2.VideoCapture(0) # TODO move me out
        start = time.time()
        ret, frame = cam.read()
        #print(f".read frame: {time.time()-start}")
        #cv2.imshow('image', frame) # show image in gui
        #pdb.set_trace()
        # DETTE VIRKER IKKE! cam henter ikke ny frame ved read 2. gang.
        #_, frame = cam.read()

        #print("cam read "+str(_))

   
        #cv2.imwrite('mypicture.jpg', frame)
        #filename = os.getcwd()+os.sep+"cap/0.jpg" # REMEMBER TO MANUALLY CREATE DIR

        ret, buffer = cv2.imencode('.jpg', frame)
#        print(f"convert frame to buffer: {time.time()-start}")

        # write buffer to file
        #file=open('/home/itk/jetson_skodrobot/cap/my_cool_buffer.jpg', 'wb')
        #file.write(buffer)
        cv2.imwrite('/home/itk/jetson_skodrobot/cap/my_cool_buffer.jpg', frame)
        #file.close()
 #       print(f"save buffer to filesystem: {time.time()-start}")

#        if cv2.waitKey(1) & 0xFF == ord('q'):
#            exit(0)
#            cam.release() #clean up
#            cv2.destroyAllWindows()
    

        answer = remote_prediction(buffer)
 #       print(type(answer))
 #       print(answer['data'])
   #     print(f"get result from remote: {time.time()-start}")
        if(len(answer['data']['bounding-boxes'])==0):
            print("Ingen cigaretter fundet")
        else:
            print(answer['data']['bounding-boxes'][0]['point_in_plane'])
        
  #      print(f"3 second sleep: {time.time()-start}")
        # write image (npArray) to file
        #cv2.imwrite(filename=filename,
        #            img=frame)
        del buffer, frame

    cam.release() #clean up
    cv2.destroyAllWindows()
    client.loop_stop()
    exit(0)


# See PyCharm help at https://www.jetbrains.com/help/pycharm/
