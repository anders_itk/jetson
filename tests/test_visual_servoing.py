
import json
import unittest.mock
import math


class VisualServoProcess(unittest.TestCase):
    def test_find_closest_object(self):
        import sys
        from tests.test_data.inference_sample import INFERENCE
        #sys.modules['cv2'] = unittest.mock.Mock() # dont import cv2
        #sys.modules['main'] = unittest.mock.Mock() # dont import paho MQTT, etc.
        #from vision import get_closet_object
        print(get_closet_object(INFERENCE))

#TODO UNACCEPTABLE!
# import cv2 prevetns importing functions from vision.py. Fix later.

def get_closet_object(response: json) -> tuple:
    """
    # poor, quick-n-dirty approximation of the object closest to the robot.
    Given a list of objects, determine which object is closest to the robot,
    and return the bounding box left and right side (in pixels).
    :param object_list:
    :return: tuple: (int, int). Bounding box left and right pixels.
    """

    obj_centroids = []
    for obj in response['data']['bounding-boxes']:
        left = obj['coordinates']['left']
        right = obj['coordinates']['right']
        top = obj['coordinates']['top']
        bottom = obj['coordinates']['bottom']
        centroid_y = round((right + left) / 2)
        centroid_x = round((bottom + top) / 2)
        obj_centroids.append((obj, afstand(centroid_x, centroid_y)))  # TODO self.afstand is untested.

    sorted(obj_centroids, key=lambda centroid: centroid[1])

    print(obj_centroids[0])


def afstand(x, y):
    # euclidian distance from center bottom of image
    return math.sqrt(((x - 640) ** 2) + ((y - 720) ** 2))

if __name__ == '__main__':
    unittest.main()
