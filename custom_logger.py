import logging.handlers
# init logging
logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger('skodrobot') #create new logger

# create console handler with a higher log level
ch = logging.StreamHandler()
# create formatter and add it to the handlers
formatter = logging.Formatter('%(asctime)s -[%(filename)s:%(lineno)d] - %(levelname)s - %(message)s')
ch.setFormatter(formatter)

rfh = logging.handlers.RotatingFileHandler('skodrobot' + '.log', maxBytes=200000, backupCount=4)
rfh.setFormatter(formatter)

# add the handlers to the logger
logger.addHandler(ch)
logger.addHandler(rfh)
logger.propagate = False