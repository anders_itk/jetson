# MIT License
# Copyright (c) 2019 JetsonHacks
# See license
# Using a CSI camera (such as the Raspberry Pi Version 2) connected to a
# NVIDIA Jetson Nano Developer Kit using OpenCV
# Drivers for the camera and OpenCV are included in the base image

import cv2

# gstreamer_pipeline returns a GStreamer pipeline for capturing from the CSI camera
# Defaults to 1280x720 @ 60fps
# Flip the image by setting the flip_method (most common values: 0 and 2)
# display_width and display_height determine the size of the window on the screen


# Anders Krogsager 15/9/2021

def get_mipi_capture_device(cam_rotation: int, sensor_id: int) -> cv2.VideoCapture:
    """
    This method returns a MIPI capture device.
    Call cap.release() before terminating.
    :param cam_rotation: int how to rotoate image
    :param sensor_id: which camera to use
    :return: video capture object
    """
    cap = cv2.VideoCapture(gstreamer_pipeline(flip_method=cam_rotation, sensor_id=sensor_id), cv2.CAP_GSTREAMER)
    return cap


def gstreamer_pipeline(
    sensor_id=0,
    capture_width=1280,
    capture_height=720,
    display_width=1280,
    display_height=720,
    framerate=60,
    flip_method=0) -> str:
    """
    This method returns a formatted string to be used by gstreamer.
    :return: a string with video configurations
    """

    return (
        "nvarguscamerasrc "
        "sensor-id=%d ! "
        "video/x-raw(memory:NVMM), "
        "width=(int)%d, height=(int)%d, "
        "format=(string)NV12, framerate=(fraction)%d/1 ! "
        "nvvidconv flip-method=%d ! "
        "video/x-raw, width=(int)%d, height=(int)%d, format=(string)BGRx ! "
        "videoconvert ! "
        "video/x-raw, format=(string)BGR ! appsink"
        % (
            sensor_id,
            capture_width,
            capture_height,
            framerate,
            flip_method,
            display_width,
            display_height,
        )
    )


def show_camera():
    """
    Show a window with real time video from the MIPI camera.
    This method is blocking and runs forever, unless window is closed or press Esc.
    """

    # To flip the image, modify the flip_method parameter (0 and 2 are the most common)
    print(gstreamer_pipeline(flip_method=0))
    cap = cv2.VideoCapture(gstreamer_pipeline(flip_method=0), cv2.CAP_GSTREAMER)
    print(f"capture device is opened: {cap.isOpened()}")
    window_handle = cv2.namedWindow("CSI Camera", cv2.WINDOW_AUTOSIZE)
    # Window
    while cv2.getWindowProperty("CSI Camera", 0) >= 0:
        ret_val, img = cap.read()
        cv2.imshow("CSI Camera", img)
        # This also acts as
        keyCode = cv2.waitKey(30) & 0xFF
        # Stop the program on the ESC key
        if keyCode == 27:
            break
    cap.release()
    cv2.destroyAllWindows()

if __name__ == "__main__":
    show_camera()

