import os
import signal
import time

import pdb

from mipi_cap import get_mipi_capture_device
from config import Development as config
import cv2
from main import Inference
from io import BytesIO


class Main():
    """
    Get image from server.
    Stop process with Ctrl+C.
    """

    def __init__(self):
        self.run = True
        signal.signal(signal.SIGINT, self.handler)

        # get camera
        self.cam = get_mipi_capture_device(cam_rotation=config.CAM_ROTATION, sensor_id=0)

        # prepare for inference
        self.inference = Inference(config.PRIMARY_CAM)



    def handler(self,signum, frame):
        self.run=False

    def start(self):
        while self.run:
            timer = time.time()
            read_ok, frame = self.cam.read()
            if read_ok:
                _, buffer = cv2.imencode('.jpg', frame)
                resp = self.inference.live_demo(img=BytesIO(buffer))

                path = os.getcwd()+os.sep+"live_demo.png"
                with open(path, 'wb') as file:
                    file.write(resp.content) # resp.text is image, resp.content is entire, response
            else:
                print("Failed to read image from camera.")

            # loiter until delay time is reached
            while (time.time() < timer+float(3)) and self.run:
                self.cam.grab() # waste buffer frames
                """
                it’s always the same solution            
                spawn a thread
                thread reads frames
                thread keeps latest frame around
                other code uses latest frame (or waits for an update)


                or check
                https://stackoverflow.com/questions/43665208/how-to-get-the-latest-frame-from-capture-device-camera-in-opencv
                """

    def clean_up(self):
        self.cam.release()

if __name__ == "__main__":
    main = Main()
    try:
        main.start()
    finally:
        main.clean_up()

    exit(0)