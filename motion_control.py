"""
This program is a test of controlling Capra P4 movement with MQTT messages.
The motion is controlled by sending a velocty command at 10 Hz.

MQTT command:

"""


# TODO
#SKIP find mqtt server for testing locally
#KISS: while loop sleep for 200ms, then publish.
#Run the loop for 1 second, with low velicity.

# TODO slow forward/backward, minimum velocity?
import paho.mqtt.publish as publish
from config import Development as config

import pdb
import time
import json
class Move():
    def __init__(self):
        """
        For other testing these are the movement parameters. linear x for velocity. angular z for turning:
        message = {"header":{"frame_id":"frame_id"},"twist":{"linear":{"x":0.05,"y":0.0,"z":0.0},"angular":{"x":0.0,"y":0.0,"z":0.0}}}
        payload = json.dumps(message)
        topic = 'capra/remote/direct_velocity'
        """


    def to(self, velocity: float, angle: float, duration: float):
        """
        Send motion commands. Blocking function.
        :param velocity: float in meters per second
        :param angle: float in radian. Negative= right, positive = left.
        :param duration: Movement time in seconds.
        """
        velocity = safe_velocity(velocity)
        duration = safe_duration(duration)

        payload = json.dumps({"header": {"frame_id": "frame_id"},
                   "twist": {"linear": {"x": velocity, "y": 0.0, "z": 0.0}, "angular": {"x": 0.0, "y": 0.0, "z": angle}}})

        # Send 10 mqtt messages per second
        pause = 0.1  # 10Hz
        loop_count = round(duration * 10)

        # start moving
        script_start = time.time()
        loop_time = time.time()
        for x in range(loop_count):
            loop_time += pause
            print(f"direct velocity at {(time.time()-script_start).__round__(3)} seconds")
            publish.single(topic='capra/remote/direct_velocity', payload=payload, hostname=config.BROKER_IP)
            while loop_time > time.time():
                pass

def safe_velocity(v):
    """
    reduces velocity
    :param v: velocity as float
    :return: velocity as float capped at 0.1. Between -0,1..0,1.
    """
    if abs(v) > 0.1:
        print("OVERSPEED. Reducing velocity.")
        if v < 0.0:
            v = -0.1
        else:
            v = 0.1
    return v

def safe_duration(d):
    """
    Reduce time and avoid negative time
    :param d: duration in seconds as float
    :return: duration as float. Capped at 5 seconds
    """
    d = abs(d)  # avoid negative time
    if d > 5:
        print("duration reduced to 5 sec")
        d = 5
    return d

def angle_smoothing(part, whole, angle):
    """
    Caluclates a fraction between 0.01 and 1.0
    Then smoothes out the angle

    :param part: loop x
    :param whole: loop_count
    :param angle: the radian angle to drive
    :return: reduced angle, moving towards 0.0
    """
    fraction =  round((abs(part - whole) / whole), 2)
    if fraction < 0.3: # how soon should the smoothing happen?
        smoothed_angle = round((angle * fraction), 3)
    else:
        smoothed_angle = angle
    return smoothed_angle

if __name__ == '__main__':
    # for development only
    move = Move()
    move.to(velocity=0.00, duration=2, angle=1.57)


# 0
# 10
